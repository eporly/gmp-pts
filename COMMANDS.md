COMMAND LIST
=============
 
 *    `/createnpc <Name>`
 *    `/spawnmonster <instance>`
 *    `/respawn <id> or focus an NPC.`
 *    `/destroynpc <Name>`
 *    `/isnpc <id>`
 *    `/killtimer <id>`
 *    `/istimeractive <id>`
 *    `/destroyitem <itemid>`
 *    `/giveitem <id> <iteminstance> <amount>`
 *    `/equipitem <id> <iteminstance>`
 *    `/unequipitem <id> <iteminstance>`
 *    `/removeitem <id> <iteminstance> <amount>`
 *    `/advremoveitem <id> <iteminstance> <amount>`
 *    `/dropitem <id> <iteminstance> <amount>`
 *    `/getplayeritem <id> <slot>`
 *    `/getworlditem <id>`
 *    `/getallequipped <id>`
 *    `/vobsetvisual <id> <visual.3ds>`
 *    `/vobsetvisible <id> <0|1>`
 *    `/vobdestroy <id>`
 *    `/mobsetname <id> <newname>`
 *    `/mobsetobjectname <id> <objectname>`
 *    `/mobsetvisual <id> <visual.3ds>`
 *    `/mobsetscheme <id> <mobtype(int)> <scheme>`
 *    `/mobsetusewithitem <id> <iteminstance>`
 *    `/mobsetvisible <id> <0|1>`
 *    `/mobsetcollision <id> <0|1>`
 *    `/mobsetposition <id> <x> <y> <z>`
 *    `/mobsetrotation <id> <rot_x> <rot_y> <rot_z>`
 *    `/mobdestroy <id>`
 *    `/clearinventory <id>`
 *    `/setcamerabehindplayer <id> <id>`
 *    `/setcamerabehindvob <id> <vobid>`
 *    `/setdefaultcamera <id>`
 *    `/freezecamera <id> <0|1>`
 *    `/setplayerwalk <id> <walkstyle.mds>`
 *    `/getplayerwalk <id>`
 *    `/removeplayeroverlay <id> <overlay>`
 *    `/playanimation <id> <animation>`
 *    `/settime <hour> <minute>`
 *    `/setweather <weather> <lightning> <startHour> <startMinute> <endHour> <endMinute>`
 *    `/setplayervirtualworld <id> <world(int)>`
 *    `/getplayervirtualworld <id>`
 *    `/setvirtualworldforallconnected <virtualworld(int)>`
 *    `/kick <id>`
 *    `/ban <id>`
 *    `/sendplayermessage <id> <msg>`
 *    `/sendmessagetoall <global>`
 *    `/setnicknamefont <font.tga>`
 *    `/getdistance2d <x1> <z1> <x2> <z2>`
 *    `/getdistance3d <x1> <y1> <z1> <x2> <y2> <z2>`
 *    `/getdistanceplayers <id> <id>`
 *    `/getangletopos <x1> <z1> <x2> <z2>`
 *    `/getplayerangleto <id> <x> <z>`
 *    `/completeheal <id>`
 *    `/setplayercolor <id> <r> <g> <b>`
 *    `/getplayercolor <id>`
 *    `/setplayermaxhealth <id> <value>`
 *    `/getplayermaxhealth <id>`
 *    `/setplayerhealth <id> <value>`
 *    `/getplayerhealth <id>`
 *    `/setplayermaxmana <id> <value>`
 *    `/getplayermaxmana <id>`
 *    `/setplayermana <id> <value>`
 *    `/getplayermana <id>`
 *    `/setplayerstrength <id> <value>`
 *    `/getplayerstrength <id>`
 *    `/setplayerdexterity <id> <value>`
 *    `/getplayerdexterity <id>`
 *    `/setplayerpos <id> <x> <y> <z>`
 *    `/getplayerpos <id>`
 *    `/setplayerangle <id> <value>`
 *    `/getplayerangle <id>`
 *    `/setplayername <id> <name>`
 *    `/getplayername <id>`
 *    `/setplayerinstance <id> <instance>`
 *    `/getplayerinstance <id>`
 *    `/setplayerskillweapon <id> <value>`
 *    `/getplayerskillweapon <id>`
 *    `/setplayerweaponmode <id> <value>`
 *    `/getplayerweaponmode <id>`
 *    `/setplayeradditionalvisual <id> <bodymodel> <bodytexid> <headmodel> <headtexid>`
 *    `/getplayeradditionalvisual <id>`
 *    `/setplayerfatness <id> <value>`
 *    `/getplayerfatness <id>`
 *    `/setplayeracrobatic <id> <0|1>`
 *    `/getplayeracrobatic <id>`
 *    `/setplayerlevel <id> <value>`
 *    `/getplayerlevel <id>`
 *    `/setplayerexperience <id> <value>`
 *    `/getplayerexperience <id>`
 *    `/setplayerexperiencenextlevel <id> <value>`
 *    `/getplayerexperiencenextlevel <id>`
 *    `/setplayerlearnpoints <id> <value>`
 *    `/getplayerlearnpoints <id>`
 *    `/freezeplayer <id> <0|1>`
 *    `/equiparmor <id> <ItAr_...>`
 *    `/unequiparmor <id> <ItAr_...>`
 *    `/equiphelmet <id> <ItAr_...>`
 *    `/unequiphelmet <id> <ItAr_...>`
 *    `/equipbelt <id> <ItBe_...>`
 *    `/unequipbelt <id> <ItBe_...>`
 *    `/equipmeleeweapon <id> <ItMw_...>`
 *    `/unequipmeleeweapon <id> <ItMw_...>`
 *    `/equiprangedweapon <id> <ItRw_...>`
 *    `/unequiprangedweapon <id> <ItRw_...>`
 *    `/setplayerworld <id> <world.zen> <waypoint>`
 *    `/getplayerworld <id>`
 *    `/setplayermagiclevel <id> <0-6>`
 *    `/getplayermagiclevel <id>`
 *    `/setplayerscience <id> <value>`
 *    `/getplayerscience <id>`
 *    `/setplayergold <id> <value>`
 *    `/getplayergold <id>`
 *    `/getfocus <id>`
 *    `/getplayerip <id>`
 *    `/getmacaddress <id>`
 *    `/enabledropafterdeath <0|1>`
 *    `/getlefthand <id>`
 *    `/getrighthand <id>`
 *    `/setplayerscale <id> <length> <width> <height>`
 *    `/getplayerscale <id>`
 *    `/setbleedthreshold <value>`
 *    `/turnplayerto <id> <x> <z>`
 *    `/setplayerguild <id> <guildid>`
 *    `/getplayerguild <id>???`
 *    `/getplayerresolution <id>`
 *    `/getplayerhand <id>`
 *    `/setplayerhand <id> <hand: 0|1> <iteminstance>`
 *    `/healovertime <id> <value>`
 *    `/dmgovertime <id> <value>`
