# GMP-PTS

Lua scripts for the public test server

## Description

This project is intended to test new releases of the [GMP](https://gitlab.com/Reveares/GMP). 

## Requirements

* Any 64-bit Linux OS will do
* GMP Server binary
* ldoc (Documentation)
* luacheck (Linting)

## Installation

* Download the latest version of the GMP
* Clone this project
* Optional: Edit `gmp_server.ini`
* Run `make run` to start the server

## Roadmap

* Implement all native GMP-LUA Functions to be used ingame.
* Implement some fun things that help debugging
* Maybe later code that is able to host fun events

## Contributing

Ask me for project access in [Discord - Gothic Modding Community](https://discord.gg/EDz84Y45)
Create an issue and an according MR.

## Authors and acknowledgment

Thanks to Sabrosa who keeps updating the GMP.
Thanks to all interested parties and future contributors.

## License

[MIT](https://gitlab.com/simon-mueller/gmp-pts/-/blob/main/LICENSE)

## Project status

This project will grow from day to day.
