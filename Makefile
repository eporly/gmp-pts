# Usage:
# make			# lint and start the server
# make run		# start the server
# make doc		# create documentation with ldoc

.PHONY: all lint doc run
.DEFAULT_GOAL := lintandrun

all: default lint run doc

default:
	@echo "Makefile for gmp-pts"

lint:
	@luacheck . -g \
	--ignore 212 # unused variables

doc:
	@ldoc .

run:
	@gmp_server

lintandrun: lint run
