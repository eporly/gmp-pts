--- Table including all possible mobsis and their values
-- visual, name, mobtype, scheme, useWithItem, world, x, y, z, objectName
Beds =
{
	name = "MOBNAME_BED",
	scheme = "BEDHIGH",
	mobtype = OCMOBBED,
	{
		visual = "BED_1_OC.ASC",
		objectName = "BettALeins",
	},
	{
		visual = "BED_2_OC.ASC",
		objectName = "BettALzwei",
	},
	{
		visual = "BEDHIGH_1_OC.ASC",
		objectName = "hohesBettALeins",
	},
	{
		visual = "BEDHIGH_2_OC.ASC",
		objectName = "hohesBettALzwei",
	},
	{
		visual = "BEDHIGH_NW_EDEL_01.ASC",
		objectName = "edlesBettNL",
	},
	{
		visual = "BEDHIGH_NW_MASTER_01.ASC",
		objectName = "MasterBettNL",
	},
	{
		visual = "BEDHIGH_NW_NORMAL_01.ASC",
		objectName = "normalesBettNL",
	},
	{
		visual = "BEDHIGH_PC.ASC",
		objectName = "BettPC",
	},
	{
		visual = "BEDHIGH_PSI.ASC",
		objectName = "BettSL",
	},
}

Benches =
{
	name = "MOBNAME_BENCH",
	scheme = "BENCH",
	mobtype = OCMOBINTER,
	{
		visual = "BENCH_1_NC.ASC",
		objectName = "BankNL",
	},
    {
        visual = "BENCH_1_OC.ASC",
        objectName = "BankAL",
    },
    {
        visual = "BENCH_2_OC.ASC",
        objectName = "BankAL2",
    },
    {
        visual = "BENCH_3_OC.ASC",
        objectName = "BankAL3",
    },
    {
        visual = "BENCH_NW_CITY_01.ASC",
        objectName = "BankNLStadt1",
    },
    {
        visual = "BENCH_NW_CITY_02.ASC",
        objectName = "BankNLStadt2",
    },
    {
        visual = "BENCH_NW_OW_01.ASC",
        objectName = "BankNLAlteWelt1",
    },
    {
        visual = "BENCH_THRONE.ASC",
        objectName = "BankThron",
    },
}

Chairs =
{
	name = "MOBNAME_CHAIR",
	mobtype = OCMOBINTER,
	scheme = "CHAIR",
	{
		visual = "CHAIR_1_NC.ASC",
		objectName = "StuhlNL",
	},
	{
		visual = "CHAIR_2_NC.ASC",
		objectName = "StuhlNL2",
	},
	{
		visual = "CHAIR_1_OC.ASC",
		objectName = "StuhlAL",
	},
	{
		visual = "CHAIR_2_OC.ASC",
		objectName = "StuhlAL2",
	},
	{
		visual = "CHAIR_3_OC.ASC",
		objectName = "StuhlAL3",
	},
	{
		visual = "CHAIR_1_PC.ASC",
		objectName = "StuhlSL",
	},
	{
		visual = "CHAIR_3_PC.ASC",
		objectName = "StuhlSL3",
	},
	{
		visual = "CHAIR_NW_EDEL_01.ASC",
		objectName = "StuhlNLEdel",
	},
	{
		visual = "CHAIR_NW_NORMAL_01.ASC",
		objectName = "Stuhl",
	},
}

Doors =
{
	name = "MOBNAME_DOOR",
	scheme = "DOOR",
	mobtype = OCMOBDOOR,
	{
		visual = "DOOR_WOODEN.MDS",
		objectName = "DOOR_WOODEN",
	},
	{
		visual = "DOOR_NW_CITY_01.MDS",
		objectName = "DOOR_NW_CITY_01",
	},
	{
		visual = "DOOR_NW_DRAGONISLE_01.MDS",
		objectName = "DOOR_NW_DRAGONISLE_01",
	},
	{
		visual = "DOOR_NW_DRAGONISLE_02.MDS",
		objectName = "DOOR_NW_DRAGONISLE_02",
	},
	{
		visual = "DOOR_NW_NORMAL_01.MDS",
		objectName = "DOOR_NW_NORMAL_01",
	},
	{
		visual = "DOOR_NW_POOR_01.MDS",
		objectName = "DOOR_NW_POOR_01",
	},
	{
		visual = "DOOR_NW_RICH_01.MDS",
		objectName = "DOOR_NW_RICH_01",
	},
}

Graves =
{
	scheme = "GRAVE",
	mobtype = OCMOBINTER,
	{
		name = "MOBNAME_GRAVE_01",
		visual = "GRAVE_ORC_1.MDS",
		objectName = "Orkgrab1",
	},
	{
		name = "MOBNAME_GRAVE_02",
		visual = "GRAVE_ORC_2.MDS",
		objectName = "Orkgrab2",
	},
	{
		name = "MOBNAME_GRAVE_08",
		visual = "GRAVE_ORC_3.MDS",
		objectName = "Orkgrab3",
	},
	{
		name = "MOBNAME_GRAVE_10",
		visual = "GRAVE_ORC_4.MDS",
		objectName = "Orkgrab4",
	},
}

Ladders =
{
	scheme = "LADDER",
	mobtype = OCMOBLADDER,
	{
		name = "LADDER_1",
		visual = "LADDER.ASC",
		objectName = "LADDER_1",
	},
	{
		name = "LADDER_2",
		visual = "LADDER_2.ASC",
		objectName = "LADDER_2",
	},
	{
		name = "LADDER_3",
		visual = "LADDER_3.ASC",
		objectName = "LADDER_3",
	},
	{
		name = "LADDER_4",
		visual = "LADDER_4.ASC",
		objectName = "LADDER_4",
	},
	{
		name = "LADDER_5",
		visual = "LADDER_5.ASC",
		objectName = "LADDER_5",
	},
	{
		name = "LADDER_6",
		visual = "LADDER_6.ASC",
		objectName = "LADDER_6",
	},
	{
		name = "LADDER_7",
		visual = "LADDER_7.ASC",
		objectName = "LADDER_7",
	},
	{
		name = "LADDER_8",
		visual = "LADDER_8.ASC",
		objectName = "LADDER_8",
	},
	{
		name = "LADDER_9",
		visual = "LADDER_9.ASC",
		objectName = "LADDER_9",
	},
}

Smallchests =
{
	name = "MOBNAME_CHEST",
	scheme = "CHESTSMALL",
	mobtype = OCMOBCONTAINER,
	{
		visual = "CHESTSMALL_NW_POOR_LOCKED.MDS",
		objectName = "kleineKistemitSchlossG2",
		useWithItem = "ITKE_LOCKPICK",
	},
	{
		visual = "CHESTSMALL_NW_POOR_OPEN.MDS",
		objectName = "kleineKisteG2",
	},
	{
		visual = "CHESTSMALL_OCCHESTSMALL.MDS",
		objectName = "kleineKisteG1",
	},
	{
		visual = "CHESTSMALL_OCCHESTSMALLLOCKED.MDS",
		objectName = "kleineKistemitSchlossG1",
		useWithItem = "ITKE_LOCKPICK",
	},
}

Bigchests =
{
	name = "MOBNAME_CRATE",
	scheme = "CHESTBIG",
	mobtype = OCMOBCONTAINER,
	{
		visual = "CHESTBIG_NW_NORMAL_LOCKED.MDS",
		objectName = "grosseKistemitSchlossG2",
		useWithItem = "ITKE_LOCKPICK",
	},
	{
		visual = "CHESTBIG_NW_NORMAL_OPEN.MDS",
		objectName = "grosseKisteG2",
	},
	{
		visual = "CHESTBIG_NW_RICH_OPEN.MDS",
		objectName = "grosseKisteG1",
	},
	{
		visual = "CHESTBIG_NW_RICH_LOCKED.MDS",
		objectName = "grosseKistemitSchlossG1",
		useWithItem = "ITKE_LOCKPICK",
	},
	{
		visual = "CHESTBIG_ADD_STONE_LOCKED.MDS",
		objectName = "grosseKisteJharkendar",
	},
}

Touchplates =
{
	name = "TOUCHPLATE",
	mobtype = OCMOBINTER,
	scheme = "TOUCHPLATE",
	{
		visual = "TOUCHPLATE_SEQ_STONE_KEY_01.mds",
		objectName = "Steinschluessel",
	},
	{
		visual = "TOUCHPLATE_SEQ_STONE_MEN_01.mds",
		objectName = "Steinmann",
	},
	{
		visual = "TOUCHPLATE_SEQ_STONE_STORM_01.mds",
		objectName = "Steinsturm",
	},
	{
		visual = "TOUCHPLATE_SEQ_STRONE_SUN_01.mds",
		objectName = "Steinsonne",
	},
	{
		visual = "TOUCHPLATE_SEQ_STONE_TRAP_01.mds",
		objectName = "Steinfalle",
	},
	{
		visual = "TOUCHPLATE_STONE.mds",
		objectName = "Tischplatte",
	},
}

Crafting =
{
	mobtype = OCMOBINTER,
	{
		name = "MOBNAME_CAULDRON",
		visual = "CAULDRON_OC.ASC",
		scheme = "CAULDRON",
		useWithItem = "ITMI_SCOOP",
		objectName = "Kessel",
	},
	{
		name = "MOBNAME_SAW",
		visual = "BAUMSAEGE_1.ASC",
		scheme = "BAUMSAEGE",
		useWithItem = "ITMI_SAW",
		objectName = "Baumsaege",
	},
	{
		name = "MOBNAME_ANVIL",
		visual = "BSANVIL_OC.mds",
		scheme = "BSANVIL",
		useWithItem = "ITMW_1H_MACE_L_04",
		objectName = "AmbossAL",
	},
	{
		name = "MOBNAME_BUCKET",
		visual = "BSCOOL_OC.mds",
		scheme = "BSCOOL",
		useWithItem = "ITMIBLADERAWHOT",
		objectName = "Wasser-Eimer",
	},
	{
		name = "MOBNAME_FORGE",
		visual = "BSFIRE_OC.mds",
		scheme = "BSFIRE",
		useWithItem = "ITMISWORDRAW",
		objectName = "SchmiedeAL",
	},
	{
		name = "MOBNAME_GRINDSTONE",
		visual = "BSSHARP_OC.mds",
		scheme = "BSSHARP",
		useWithItem = "ITMIBLADERAW",
		objectName = "Schleifstein",
	},
	{
		name = "MOBNAME_WINEMAKER",
		visual = "HERB_PSI.mds",
		scheme = "HERB",
		useWithItem = "ITMI_STOMPER",
		objectName = "Krautstampfer",
	},
	{
		name = "MOBNAME_LAB",
		visual = "LAB_PSI.ASC",
		scheme = "LAB",
		useWithItem = "ITMI_FLASK",
		objectName = "AlchemietischSL",
	},
	{
		name = "MOBNAME_PAN",
		visual = "PAN_OC.mds",
		scheme = "PAN",
		useWithItem = "ITFO_FISH",
		objectName = "Pfanne",
	},
	{
		name = "MOBNAME_RUNEMAKER",
		visual = "RMAKER_1.mds",
		scheme = "RMAKER",
		useWithItem = "ITMI_RUNEBLANK",
		objectName = "Runentisch",
	},
	{
		name = "MOBNAME_STOVE",
		visual = "STOVE_NW_CITY_01.ASC",
		scheme = "STOVE",
		useWithItem = "ITMI_PANFULL",
		objectName = "Herd",
	},
}

Ores =
{
	name = "MOBNAME_ORE",
	mobtype = OCMOBINTER,
	scheme = "ORE",
	{
		visual = "ORE_GROUND.ASC",
		objectName = "Erzader",
		useWithItem = "ItMw_2H_Axe_L_01",
	},
	{
		name = "MOBNAME_ADDON_GOLD",
		visual = "ORE_GROUND_GOLD.ASC",
		objectName = "Goldader",
		useWithItem = "ItMw_2H_Axe_L_02",
	},
	{
		visual = "ORE_GROUND_IRON.ASC",
		objectName = "Eisenader",
		useWithItem = "ItMw_2H_Axe_L_03",
	},
}

Mobs =
{
	name = "MOBNAME_DEFAULT",
	mobtype = OCMOBINTER,
	scheme = "",
	{
		name = "MOBNAME_BARBQ_SHEEP",
		visual = "BARBQ_NW_MISC_SHEEP_01.mds",
		scheme = "BARBQ",
		--useWithitem = "ITFOMUTTONRAW",
		objectName = "NLDrehspiess",
	},
	{
		name = "MOBNAME_BARBQ_SCAV",
		visual = "BARBQ_Scav.mds",
		scheme = "BARBQ",
		objectName = "ScavengerDrehspiess",
	},
	{
		name = "MOBNAME_BOOKSTAND",
		visual = "BOOK_BLUE.mds",
		scheme = "BOOK",
		objectName = "Buchstaender",
	},
	{
		name = "MOBNAME_BOOKSBOARD",
		visual = "BOOK_NW_CITY_CUPBOARD_01.ASC",
		scheme = "BOOK",
		objectName = "Buchkommode",
		offset = 140,
	},
	{
		name = "FIREPLACE_GROUND2",
		visual = "FIREPLACE_GROUND2.mds",
		mobtype = OCMOBINTER,
		scheme = "PAN",
		useWithItem = "ITLSTORCHBURNING",
		objectName = "Kamin1",
	},
	{
		name = "FIREPLACE_HIGH2",
		visual = "FIREPLACE_GROUND2.mds",
		mobtype = OCMOBINTER,
		scheme = "CHESTSMALL",
		useWithItem = "ITLSTORCH",
		objectName = "Kamin2",
	},
	{
		name = "MOBNAME_SECRETSWITCH",
		visual = "LEVER_1_OC.mds",
		scheme = "LEVER",
		--useWithItem = "ITMISTAMPFER", TODO
		objectName = "SchalterAL",
		offset = 150,
	},
	{
		name = "MOBNAME_WATERPIPE",
		visual = "SMOKE_WATERPIPE.mds",
		scheme = "SMOKE",
		--useWithItem = "ITMISTAMPFER", TODO
		objectName = "Wasserpfeife",
	},
	{
		name = "MOBNAME_ADDON_SOCKEL",
		visual = "TURNSWITCH_BLOCK.mds",
		scheme = "TURNSWITCH",
		--useWithItem = "ITKE_BURGTOR_AL",
		objectName = "Drehschalter",
	},
	{
		name = "VWHEEL",
		visual = "VWHEEL_1_OC.mds",
		scheme = "VWHEEL",
		useWithItem = "ITKE_BURGTOR_AL",
		objectName = "Winde",
	},
	{
		name = "MOBNAME_REPAIR",
		visual = "REPAIR_PLANK.ASC",
		scheme = "REPAIR",
		useWithItem = "ITMI_HAMMER",
		objectName = "Reparaturplanke",
		offset = 150,
	},
	{
		name = "FLAGGE",
		visual = "FLAG_MESH.ASC",
		scheme = "FLAG",
		objectName = "Flagge",
	},
	{
		name = "MOBNAME_INNOS",
		visual = "INNOS_NW_MISC_01.ASC",
		scheme = "INNOS",
		objectName = "Innos-Statue",
	},
	{
		name = "MOBNAME_IDOL",
		visual = "IDOL_SLEEPER3_PC.ASC",
		scheme = "IDOL",
		objectName = "Schlaeferstatuette",
	},
	{
		name = "MOBNAME_VOBBOX",
		visual = "VOBBOX_1.ASC",
		scheme = "VOBBOX",
		useWithItem = "ITMI_ALARMHORN",
		objectName = "VOBBOX",
		offset = 150,
	},
	{
		visual = "Waypoint.ASC",
		objectName = "Waypoint",
		offset = 150,
	},
	{
		visual = "STARTPOINT.ASC",
		objectName = "Startpoint",
		offset = 150,
	},
	{
		visual = "SPOT.ASC",
		objectName = "Spot",
		offset = 150,
	},
}

Thrones = {
	name = "MOBNAME_THRONE",
	mobtype = OCMOBINTER,
	scheme = "BENCH",
	{
		visual = "THRONE_BIG.ASC",
		objectName = "grosserThron",
	},
	{
		visual = "THRONE_NW_CITY_01.ASC",
		objectName = "NLThron",
	},
	{
		visual = "OC_THRONE_GROUND.ASC",
		objectName = "ALThron",
	},
	{
		visual = "ORC_MASTERTHRONE.ASC",
		objectName = "OrkThron",
	},
}

Barrels = {
	name = "BARREL",
	mobtype = OCMOBINTER,
	scheme = "BENCH",
	{
		visual = "NW_CITY_BARREL_01.ASC",
		objectName = "V1Fass",
	},
	{
		visual = "NW_HARBOUR_BARREL_01.ASC",
		objectName = "V2Fass",
		scheme = "CHAIR",
	},
}

SpawnedMobs = {}; -- contains userdata variables

function mobCreateAll(playerid, params)
	local valid, category = sscanf(params, "s");
	if valid ~= 1 then
		SendPlayerMessage(playerid,255,0,0, "Category not given. Choices: Beds, Benches, Chairs, Doors, Graves, \
		Ladders, Smallchests, Bigchests, Touchplates, Crafting, Ores, Mobs, Thrones");
		return
	else
		mobtable = _G[category];
		if mobtable == nil then
			SendPlayerMessage(playerid,255,0,0, "Category unknown.");
			return
		end
	end
	local incr = 0;
	for i = 1, #mobtable do
		local world = GetPlayerWorld(playerid);
		local x,y,z = GetPlayerPos(playerid);
		if type(mobtable[i].name) ~= "string" then
			mobtable[i].name = mobtable.name;
		end
		if type(mobtable[i].scheme) ~= "string" then
			mobtable[i].scheme = mobtable.scheme;
		end
		if not(mobtable[i].mobtype) then
			mobtable[i].mobtype = mobtable.mobtype;
		end
		if type(mobtable[i].useWithItem) ~= "string" then
			mobtable[i].useWithItem = "";
		end
		if type(mobtable[i].offset) ~= "number" then
			mobtable[i].offset = 0;
		end
		if type(mobtable[i].x) ~= "number" or type(mobtable[i].y) ~= "number"
		or type(mobtable[i].z) ~= "number" or type(mobtable[i].world) ~= "string" then
			x=x + 50;
			y=y - 90 + mobtable[i].offset;
			z=z - 50 + incr;
		end
		local userdata = Mob.Create(mobtable[i].visual,mobtable[i].name,mobtable[i].mobtype,mobtable[i].scheme,
mobtable[i].useWithItem,world,x,y,z,mobtable[i].objectName);
		table.insert(SpawnedMobs, userdata);
	SendPlayerMessage(playerid, 255,255,255,
	"Mob "..mobtable[i].objectName..": "..Mob.GetGUID(userdata).."ID: "..#SpawnedMobs.." offset: "..mobtable[i].offset);
		Mob.SetRotation(userdata,0,GetPlayerAngle(playerid) - 180,0);
		incr = incr + 250;
		if mobtable[i].useWithItem ~= "" then
			GiveItem(playerid, mobtable[i].useWithItem, 1);
		end
		if string.contains(category, "chest") then
			OpenLocks(1);
		end
	end
end


print(debug.getinfo(1).source .. " has been loaded.");
