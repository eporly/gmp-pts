-- Textures
Targas = {
	{ filename="ADDON_WORLD",				start_x=0,	start_y=0,		width=8192,	height=8192 },
	{ filename="ADDON_WORLD_TREASURE",		start_x=0,	start_y=0,		width=8192,	height=8192 },
	{ filename="MAP_NEWCAMP",				start_x=0,	start_y=0,		width=8192,	height=8192 },
	{ filename="MAP_NEWWORLD",				start_x=0,	start_y=0,		width=8192,	height=8192 },
	{ filename="MAP_NWCITY",				start_x=0,	start_y=0,		width=8192,	height=8192 },
	{ filename="FOCUS_HIGHLIGHT",			start_x=7168,	start_y=2048,	width=0,	height=4096 },
	{ filename="LOG_BACK",					start_x=2048,	start_y=2048,	width=4096,	height=4096 },
	{ filename="CONSOLE",					start_x=0,		start_y=7680,	width=8192,	height=512 },
	{ filename="L",							start_x=1905,	start_y=4445,	width=160,	height=160 },
	{ filename="R",							start_x=1905,	start_y=4445,	width=160,	height=160 },
	{ filename="O",							start_x=1905,	start_y=4445,	width=160,	height=160 },
	{ filename="U",							start_x=1905,	start_y=4445,	width=160,	height=160 },
	{ filename="LO",						start_x=1905,	start_y=4445,	width=160,	height=160 },
	{ filename="RO",						start_x=1905,	start_y=4445,	width=160,	height=160 },
	{ filename="LU",						start_x=1905,	start_y=4445,	width=160,	height=160 },
	{ filename="RU",						start_x=1905,	start_y=4445,	width=160,	height=160 },
}

function CreateTextures()
	Textures = {};
	for i=1, #Targas do
		local filename = Targas[i].filename;
		local x = Targas[i].start_x;
		local y = Targas[i].start_y;
		local w = Targas[i].width;
		local h = Targas[i].height;
		Textures[filename] = CreateTexture(x,y,x+w,y+h, filename..".TGA");
	end
end

CreateTextures();

function CreateCompass(playerid)
	Player[playerid].compass = CreateTexture(4096,0,4096+160,0+160, "O.TGA");
end

function UpdateCompass(playerid)
	local dir = quoVadis(GetPlayerAngle(playerid))
	if dir ~= nil then
		UpdateTexture(Player[playerid].compass, 4096,1536,4096+108,1536+192, dir..".TGA");
	end
end


print(debug.getinfo(1).source .. " has been loaded.");
