function OnPlayerResponseItem(playerid, slot, itemInstance, amount, equipped)
	if equipped == 1 then
		SendMessageToAll(0,191,255, string.format("Player %s has %dx %s in slot %d and it's equipped.",
		 GetPlayerName(playerid), amount, itemInstance, slot));
	else
		SendMessageToAll(0,191,255, string.format("Player %s has %dx %s in slot %d and it's not equipped.",
		 GetPlayerName(playerid), amount, itemInstance, slot));
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
