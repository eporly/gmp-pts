function OnPlayerConnect(playerid)
	GetFileList(playerid, "data", ".mod;.vdf");
	SendPlayerMessage(playerid, 0, 255, 0, "Welcome to GMP Public Testserver");
	SpawnPlayer(playerid);
	SetPlayerEnable_OnPlayerKey(playerid, 1);
	SetPlayerEnableOnUpdate(playerid, 1);
	-- Show Draws
	ShowDraw(playerid, TimeDraw);
	ShowDraw(playerid, TickDraw);
	ShowDraw(playerid, VersionDraw);
	-- Player Vars
	Player[playerid] = {};
	Player[playerid].x,Player[playerid].y,Player[playerid].z = GetPlayerPos(playerid);
	Player[playerid].cursor = 0;
	Player[playerid].cursorsens = 3.5;
	Player[playerid].statdraws = MultiDraw(7100, 4600, CreateStatDraws(playerid));
	--Player[playerid].mapmarker = CreatePlayerDraw(playerid, 4096,4096, "X", "Font_Old_10_White_Hi.tga", 255,255,255);
	Player[playerid].mapmarker = Textures["O"];
	Player[playerid].mappos_x = 1880
	Player[playerid].mappos_z = 4515
	Player[playerid].lastbuttonpressed = 255;
	PlayerDraws[playerid] = {};
	PlayerDraws[playerid][1] = CreatePlayerDraw(playerid, 130, 6400, "Location", "Font_Old_10_White_Hi.tga", 255,255,255);
	PlayerDraws[playerid][2] = CreatePlayerDraw(playerid, 130, 6600, "X: 0", "Font_Old_10_White_Hi.tga", 255,255,255);
	PlayerDraws[playerid][3] = CreatePlayerDraw(playerid, 130, 6800, "Y: 0", "Font_Old_10_White_Hi.tga", 255,255,255);
	PlayerDraws[playerid][4] = CreatePlayerDraw(playerid, 130, 7000, "Z: 0", "Font_Old_10_White_Hi.tga", 255,255,255);
	PlayerDraws[playerid][5] = CreatePlayerDraw(playerid, 130, 7200, "A: 0", "Font_Old_10_White_Hi.tga", 255,255,255);
	PlayerDraws[playerid][6] = CreatePlayerDraw(playerid, 130, 6200, "Walkstyle", "Font_Old_10_White_Hi.tga", 255,255,255);
	PlayerDraws[playerid][7] = CreatePlayerDraw(playerid, 130, 6000, "Button", "Font_Old_10_White_Hi.tga", 255,255,255);
	for i=1, #PlayerDraws[playerid] do
		ShowPlayerDraw(playerid, PlayerDraws[playerid][i]);
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
