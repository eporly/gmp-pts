function OnPlayerTakeItem(playerid, itemid, instance, amount, x, y, z, world)
	local how = "takes";
	if itemid == -1 then
		how = "picks up";
	elseif itemid == -2 then
		how = "cheats";
	elseif itemid == -3 then
		how = "withdraws";
	end
	local str = string.format("Player %s %s %dx %s (id: %d) at %d,%d,%d in %s",
	GetPlayerName(playerid), how, amount, instance, itemid, x, y, z, world);
	SendMessageToAll(0,191,255, str);
end

print(debug.getinfo(1).source .. " has been loaded.");
