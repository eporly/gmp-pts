function OnPlayerChangeHealth(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "HP", newValue, oldValue);
end
function OnPlayerChangeMaxHealth(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "MaxHP", newValue, oldValue);
end
function OnPlayerChangeMana(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "MP", newValue, oldValue);
end
function OnPlayerChangeMaxMana(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "MaxMP", newValue, oldValue);
end
function OnPlayerChangeStrength(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "Str", newValue, oldValue);
end
function OnPlayerChangeDexterity(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "Dex", newValue, oldValue);
end
function OnPlayerChangeExperience(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "Exp", newValue, oldValue);
end
function OnPlayerChangeExperienceNextLevel(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "ExpNextLvl", newValue, oldValue);
end
function OnPlayerChangeLevel(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "Lvl", newValue, oldValue);
end
function OnPlayerChangeNextLevel(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "NextLvl", newValue, oldValue);
end
function OnPlayerChangeMagicLevel(playerid, newValue, oldValue)
	OnPlayerChangeAttribute(playerid, "MagicCircle", newValue, oldValue);
end

--- OnPlayerChangeAttribute
-- nice wrapper function to display a pop-up text to the player
-- contains the name of the changed attribute followed by +/- sign and the value
-- @tparam playerid number the player whose attribute changed
-- @tparam attribute string the name of attribute
-- @tparam newValue number new value
-- @tparam oldValue number old value
function OnPlayerChangeAttribute(playerid, attribute, newValue, oldValue)
	local r1 = math.random(100,1000);
	if newValue > oldValue then
		local plus = newValue - oldValue;
		GameTextForPlayer(playerid, 4650+r1,5900+r1, attribute..": +"..plus,"Font_Old_20_White_Hi.tga",0,255,0, 1000);
		Player[playerid].lastheal = plus;
	else
		local minus = oldValue - newValue;
		GameTextForPlayer(playerid, 3350-r1,5900-r1, attribute..": -"..minus,"Font_Old_20_White_Hi.tga",255,0,0, 1000);
		Player[playerid].lastdmg = minus;
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
