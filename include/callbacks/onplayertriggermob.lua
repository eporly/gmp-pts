function OnPlayerTriggerMob(playerid, scheme, objectname, trigger)
	if trigger == 1 then
		SendMessageToAll(255,255,255, GetPlayerName(playerid).." uses "..objectname.."("..scheme..")");
		if not(scheme == "LADDER" or string.contains(scheme, "CHEST")) then
			FreezePlayer(playerid, 1);
			SendPlayerMessage(playerid, 255,255,255, "Freezing player!");
		end
		if scheme then
			GameTextForPlayer(playerid, 4096, 4096, "Craftscript "..scheme.." starting...",
			"Font_Old_20_White_Hi.tga", 255, 255, 255, 2000);
		end
	else
		SendMessageToAll(255,255,255, GetPlayerName(playerid).." leaves "..objectname.."("..scheme..")");
		FreezePlayer(playerid, 0);
		if scheme then
			GameTextForPlayer(playerid, 4096, 4096, "Craftscript "..scheme.." stopping...",
			"Font_Old_20_White_Hi.tga", 255, 255, 255, 2000);
		end
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
