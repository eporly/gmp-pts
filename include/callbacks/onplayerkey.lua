tKeys = {
	"KEY_ESCAPE",
	"KEY_1",
	"KEY_2",
	"KEY_3",
	"KEY_4",
	"KEY_5",
	"KEY_6",
	"KEY_7",
	"KEY_8",
	"KEY_9",
	"KEY_0",
	"KEY_MINUS",
	"KEY_EQUALS",
	"KEY_BACK",    --backspace
	"KEY_TAB",
	"KEY_Q",
	"KEY_W",
	"KEY_E",
	"KEY_R",
	"KEY_T",
	"KEY_Y",
	"KEY_U",
	"KEY_I",
	"KEY_O",
	"KEY_P",
	"KEY_LBRACKET",
	"KEY_RBRACKET",
	"KEY_RETURN",  --Enter on main keyboard
	"KEY_LCONTROL",
	"KEY_A",
	"KEY_S",
	"KEY_D",
	"KEY_F",
	"KEY_G",
	"KEY_H",
	"KEY_J",
	"KEY_K",
	"KEY_L",
	"KEY_SEMICOLON",
	"KEY_APOSTROPHE",
	"KEY_TILDE",       --tilde
	"KEY_LSHIFT",
	"KEY_BACKSLASH",
	"KEY_Z",
	"KEY_X",
	"KEY_C",
	"KEY_V",
	"KEY_B",
	"KEY_N",
	"KEY_M",
	"KEY_COMMA",
	"KEY_PERIOD",   --on main keyboard
	"KEY_SLASH",   --on main keyboard
	"KEY_RSHIFT",
	"KEY_MULTIPLY",   --on numeric keypad
	"KEY_LMENU",    --left Alt
	"KEY_SPACE",
	"KEY_CAPITAL",
	"KEY_F1",
	"KEY_F2",
	"KEY_F3",
	"KEY_F4",
	"KEY_F5",
	"KEY_F6",
	"KEY_F7",
	"KEY_F8",
	"KEY_F9",
	"KEY_F10",
	"KEY_NUMLOCK",
	"KEY_SCROLL",    --Scroll Lock
	"KEY_NUMPAD7",
	"KEY_NUMPAD8",
	"KEY_NUMPAD9",
	"KEY_SUBTRACT",    --on numeric keypad
	"KEY_NUMPAD4",
	"KEY_NUMPAD5",
	"KEY_NUMPAD6",
	"KEY_ADD",    -- + on numeric keypad
	"KEY_NUMPAD1",
	"KEY_NUMPAD2",
	"KEY_NUMPAD3",
	"KEY_NUMPAD0",
	"KEY_DECIMAL",   -- . on numeric keypad
	"KEY_F11",
	"KEY_F12",
	"KEY_NUMPADENTER",    -- Enter on numeric keypad
	"KEY_RCONTROL",
	"KEY_DIVIDE",    -- on numeric keypad
	"KEY_RMENU",    -- right Alt
	"KEY_PAUSE",    -- Pause
	"KEY_HOME",    -- Home on arrow keypad
	"KEY_UP",    -- UpArrow on arrow keypad
	"KEY_PRIOR",   -- PgUp on arrow keypad
	"KEY_LEFT",   -- LeftArrow on arrow keypad
	"KEY_RIGHT",    -- RightArrow on arrow keypad
	"KEY_END",    -- End on arrow keypad
	"KEY_DOWN",    -- DownArrow on arrow keypad
	"KEY_NEXT",    -- PgDn on arrow keypad
	"KEY_INSERT",    -- Insert on arrow keypad
	"KEY_DELETE",    -- Delete on arrow keypad
	"KEY_POWER",    -- System Power
	"KEY_SLEEP",    -- System Sleep
	"KEY_WAKE",    -- System Wake
}

function OnPlayerKey(playerid, keyDown, keyUp)
	if keyUp ~= "NULL" then
		Player[playerid].lastbuttonpressed = tKeys[keyUp + 1]
	end
	if keyDown == KEY_PERIOD then
		if Player[playerid].cursor == 0 then
			Player[playerid].cursorsens = 3.5;
			SetCursorSensitivity(playerid, Player[playerid].cursorsens);
			SetCursorVisible(playerid, 1);
			Player[playerid].cursor = 1;
		else
			SetCursorVisible(playerid, 0);
			Player[playerid].cursor = 0;
		end
	elseif keyDown == KEY_MINUS then
		Player[playerid].cursorsens = Player[playerid].cursorsens + 0.1;
		SetCursorSensitivity(playerid, Player[playerid].cursorsens);
	elseif keyDown == KEY_COMMA then
		Player[playerid].cursorsens = Player[playerid].cursorsens - 0.1;
		SetCursorSensitivity(playerid, Player[playerid].cursorsens);
	elseif keyDown == KEY_M then
		if Player[playerid].mapopen == nil then
			SetPlayerHand(playerid, 1, "ITWR_MAP_NWCITY");
			PlayAnimation(playerid, "S_MAP_S0");
			ShowTexture(playerid, Textures["MAP_NEWWORLD"]);
			ShowTexture(playerid, Player[playerid].mapmarker);
			--ShowPlayerDraw(playerid, Player[playerid].mapmarker);
			Player[playerid].mapopen = 1;
		else
			HideTexture(playerid, Textures["MAP_NEWWORLD"]);
			HideTexture(playerid, Player[playerid].mapmarker);
			--HidePlayerDraw(playerid, Player[playerid].mapmarker);
			Player[playerid].mapopen = nil;
			SetPlayerHand(playerid, 1, "");
			PlayAnimation(playerid, "T_MAP_S0_2_STAND");
		end
	elseif keyDown == KEY_N then
		if Player[playerid].logopen == nil then
			ShowTexture(playerid, Textures["LOG_BACK"]);
			Player[playerid].logopen = 1;
		else
			HideTexture(playerid, Textures["LOG_BACK"]);
			Player[playerid].logopen = nil;
		end
	elseif keyDown == KEY_K then
		if Player[playerid].compassopen == nil then
			ShowTexture(playerid, Player[playerid].compass);
			Player[playerid].compassopen = 1;
		else
			HideTexture(playerid, Player[playerid].compass);
			Player[playerid].compassopen = nil;
		end
	elseif keyDown == KEY_S or keyDown == KEY_DOWN or keyDown == KEY_BACKSPACE then
		FreezePlayer(playerid, 0);
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
