function OnPlayerSpellCast(playerid, itemInstance)
	local id = Player[playerid].target;
	if id ~= nil then
		SendPlayerMessage(playerid, 150,200,50, "You cast "..itemInstance.." on "..GetPlayerName(id));
	end
	if itemInstance == "ITRU_SHRINK" then
		local x,y,z = GetPlayerPos(id);
		for i = 1, 14 do
			local MagicShrink = function (p, count)
				local val = 1.0 - (count/20);
				SetPlayerScale(p, val, val, val);
				SetPlayerMaxHealth(p, (GetPlayerMaxHealth(p) * 9 / 10));
				SetPlayerHealth(p, (GetPlayerHealth(p) * 9 / 10));
				SetPlayerStrength(p, (GetPlayerStrength(p) * 9 / 10));
			end
			SetTimer(MagicShrink, 380*i, 0, id, i);
		end
		SetTimer("SetPlayerPos", 380*15, 0, id, x, y, z);
		SetTimer("SetPlayerScale", 60000, 0, id, 1, 1, 1);
	elseif itemInstance == "ITRU_SLEEP" then
		FreezePlayer(id, 1);
		PlayAnimation(id, "T_STAND_2_SLEEP");
		SetTimer("PlayAnimation", 10000, 0, id, "T_SLEEP_2_STAND");
		SetTimer("FreezePlayer", 10200, 0, id, 0);
	elseif itemInstance == "ITRU_FEAR" then
		local yourangle = GetPlayerAngle(playerid);
		local targetangle = math.abs(yourangle - 180);
		SetTimer("SetPlayerAngle", 2000, 0, id, targetangle);
		SetTimer("PlayAnimation", 2000, 0, id, "T_PLUNDER");
	elseif itemInstance == "ITRU_LIGHTNINGFLASH" then
		PlayAnimation(id, "T_STAND_2_LIGHTNING_VICTIM");
		SetTimer("PlayAnimation", 2000, 0, id, "S_FISTRUN");
	end
end

print(debug.getinfo(1).source .. " has been loaded.");
