local OPERATING_SYSTEM = 2; -- 2 Linux, 1 Windows
local QUIET = 1;

function big_require(folder, recursive, filepattern)
	if folder == nil or folder == "" then
		folder = ".";
	end

	if filepattern == nil or filepattern == "" then
		filepattern = "*";
	end

	if recursive == nil then
		recursive = true;
	end

	local files = scandir(folder, recursive, filepattern, {});

	for var = 1, #files, 1 do
		if QUIET == nil or QUIET == 0 then
			print("Loaded Lib: " .. string.sub(files[var], 0, -string.len(".lua")-1));
		end
		require(string.sub(files[var], 0, -string.len(".lua")-1));
	end
end


function scandir(directory, recursive, filepattern, t)
	if OPERATING_SYSTEM == nil or OPERATING_SYSTEM == 1 then
		for filename in io.popen('dir "' .. directory .. '\\' .. filepattern .. '.lua" /b /a-d'):lines() do
			table.insert(t, directory .. "/" .. filename);
		end
		if recursive == true then
			for dirname in io.popen('dir "' .. directory .. '" /b /ad'):lines() do
				scandir(directory .. "/" .. dirname, true, filepattern, t);
			end
		end
	elseif OPERATING_SYSTEM == 2 then
		if recursive == true then
			os.execute('find ./"' .. directory .. '" -iname "' .. filepattern .. '.lua" -type f > temp_bigrequire.tempfile');
			local handle = io.open("temp_bigrequire.tempfile");

			for filename in handle:lines() do
				table.insert(t, filename);
			end
			handle:close();
			os.remove("temp_bigrequire.tempfile");
		else
			os.execute('find ./"' .. directory .. '" -maxdepth 1 -type f -iname \
				"' .. filepattern .. '.lua" > temp_bigrequire.tempfile');
			local handle = io.open("temp_bigrequire.tempfile");
			for filename in handle:lines() do
				table.insert(t, filename);
			end
			handle:close();
			os.remove("temp_bigrequire.tempfile");
		end
	end
	return t;
end

-- String Functions

function trim(s)
	local i1, i2 = string.find(s, '^%s*');
	if i2 >= i1 then s = string.sub(s, i2+1); end
	i1, i2 = string.find(s, '%s*$');
	if i2 >= i1 then s = string.sub(s, 1, i1-1); end
	return s;
end

function string:split(delimiter)
	local result = {};
	local from = 1;
	local delim_from, delim_to = string.find(self, delimiter, from);
	while delim_from do
		table.insert(result, string.sub(self, from, delim_from - 1));
		from = delim_to + 1;
		delim_from, delim_to = string.find(self, delimiter, from);
	end
	table.insert(result, string.sub(self, from));
	return result;
end

function string.ends(str, other)
	return other == '' or string.sub(str, -string.len(other)) == other;
end

function string.starts(str, other)
	return string.sub(str, 1, string.len(other)) == other;
end

function string.contains(str, other)
	return string.find(string.lower(str), string.lower(other)) ~= nil;
end

function os.capture(cmd)
    local f = assert(io.popen(cmd, 'r'))
    local s = assert(f:read('*a'))
    f:close()
    return s
end

function xOverTime(playerid, oper, val)
	local final = 0;
	local hp = GetPlayerHealth(playerid);
	if oper == "+" then
		final = hp + val;
	elseif oper == "-" then
		final = hp - val;
	end
	SetPlayerHealth(playerid, final);
end

function table.contains(table, element)
  for _, value in pairs(table) do
	print("Comparing "..value.." with "..element);
    if value == element then
      return true
    end
  end
  return false
end

OrigRemoveItem = RemoveItem;

function AdvRemoveItem(id, instance, amount)
    local item = string.upper(instance);
    local ar = string.upper(GetEquippedArmor(id));
    local mw = string.upper(GetEquippedMeleeWeapon(id));
    local rw = string.upper(GetEquippedRangedWeapon(id));
    local he = string.upper(GetEquippedHelmet(id));
    local be = string.upper(GetEquippedBelt(id));
    if item == ar then
        UnequipArmor(id, item);
        RemoveItem(id, instance, amount+1);
        EquipArmor(id, item);
    elseif item == mw then
        UnequipMeleeWeapon(id, item);
        RemoveItem(id, instance, amount+1);
        EquipMeleeWeapon(id, item);
    elseif item == rw then
        UnequipRangedWeapon(id, item);
        RemoveItem(id, instance, amount+1);
        EquipRangedWeapon(id, item);
    elseif item == he then
        UnequipHelmet(id, item);
        RemoveItem(id, instance, amount+1);
        EquipHelmet(id, item);
    elseif item == be then
        UnequipBelt(id, item);
        RemoveItem(id, instance, amount+1);
        EquipBelt(id, item);
    else
        RemoveItem(id, instance, amount);
    end
end

--- Timer functions
--[[Timercount = 0;
OrigSetTimer = SetTimer;
OrigKillTimer = KillTimer;

local function TimerEnds(func, repeatbool, ...)
    if type(func) == "string" then
        _G[func](...);
    else
        func(...);
    end
    if repeatbool == 0 then
        Timercount = Timercount - 1;
    end
end

function SetTimer(func, time, repeatbool, ...)
    Timercount = Timercount + 1;
	return OrigSetTimer(TimerEnds, time, repeatbool, func, repeatbool, ...);
end

function KillTimer(id)
    if id ~= nil then
        if IsTimerActive(id) == 1 then
            Timercount = Timercount - 1;
            OrigKillTimer(id);
			print("Stopping timer "..id);
        end
    end
end
]]--

function UpdateMapMarker(playerid)
	local x,_,z = GetPlayerPos(playerid);
	local a = GetPlayerAngle(playerid);
	local dir = quoVadis(a);
	--print(string.format("Calling UMM for %s: a:%d mappos_x:%d mappos_z:%d",
	--GetPlayerName(playerid), a, Player[playerid].mappos_x, Player[playerid].mappos_z));
	if not(Player[playerid].lastpos_x or Player[playerid].lastpos_z) then
		Player[playerid].lastpos_x = x
		Player[playerid].lastpos_z = z
	end
	if not(playerid) then return end
	if (Player[playerid].mappos_x ~= x) and (Player[playerid].mappos_z ~= z) then -- player is moving
		local dist_x = Player[playerid].lastpos_x - x
		local dist_z = Player[playerid].lastpos_z - z
		Player[playerid].mappos_x = Player[playerid].mappos_x - dist_x / 16
		Player[playerid].mappos_z = Player[playerid].mappos_z + dist_z / 9
	end
	if dir ~= nil then
		--UpdatePlayerDraw(playerid, Player[playerid].mapmarker, math.floor(Player[playerid].mappos_x),
		--math.floor(Player[playerid].mappos_z), dir, "Font_Old_10_White_Hi.tga", 255,255,255);
		UpdateTexture(Player[playerid].mapmarker, Player[playerid].mappos_x,Player[playerid].mappos_z,
		Player[playerid].mappos_x+72,Player[playerid].mappos_z+128, dir..".TGA");
	end
	Player[playerid].lastpos_x = x;
	Player[playerid].lastpos_z = z
end


function quoVadis(angle)
	local Arrows = {
		{ name = "O",	a = 360-(45/2),	b = 360 },
		{ name = "LO",	a = 270+(45/2),	b = 360-(45/2) },
		{ name = "L",	a = 270-(45/2),	b = 325-(45/2) },
		{ name = "LU",	a = 225-(45/2),	b = 270-(45/2) },
		{ name = "U",	a = 180-(45/2),	b = 225-(45/2) },
		{ name = "RU",	a = 135-(45/2),	b = 180-(45/2) },
		{ name = "R",	a = 90 -(45/2),	b = 135-(45/2) },
		{ name = "RO",	a = 45 -(45/2),	b = 90 -(45/2) },
		{ name = "O",	a = 0,			b = 45 -(45/2) },
	}
	for i=1, #Arrows do
		if angle > Arrows[i].a and angle <= Arrows[i].b then
			return Arrows[i].name
		end
	end
	return nil
end

print(debug.getinfo(1).source .. " has been loaded.");
